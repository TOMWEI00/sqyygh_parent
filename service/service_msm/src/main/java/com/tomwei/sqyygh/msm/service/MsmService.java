package com.tomwei.sqyygh.msm.service;


import com.tomwei.sqyygh.vo.msm.MsmVo;

public interface MsmService {

// 邮箱验证码
    void sendEmail(String email, String code);
    //创建验证码
    String getCode();
    //mq发送短信
    boolean send(MsmVo msmVo);
}
