package com.tomwei.sqyygh.msm.service.impl;

import com.tomwei.sqyygh.msm.service.MsmService;
import com.tomwei.sqyygh.vo.msm.MsmVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class MsmServiceImpl implements MsmService {

    @Autowired
    private JavaMailSender javaMailSender;


    // 邮箱验证码
    @Override
    @Async
    public void sendEmail(String email, String code) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setSubject("社区医院挂号系统登录验证码");
        simpleMailMessage.setText("尊敬的:"+email+"您的注册校验验证码为：" + code + "有效期5分钟");
        simpleMailMessage.setTo(email);
        simpleMailMessage.setFrom("1430676118@qq.com");
        javaMailSender.send(simpleMailMessage);
    }

    //创建验证码
    @Override
    public String getCode() {
        int random = (int) (Math.random() * 1000000);
        System.out.println(random);
        String code = String.format("%06d", random);
        System.out.println(code);
        return code;
    }

    //mq发送短信
    @Override
    public boolean send(MsmVo msmVo) {
        if(!StringUtils.isEmpty(msmVo.getPhone())) {
//            String code = (String)msmVo.getParam().get("code");
//            boolean isSend = this.sendEmail(msmVo.getPhone(), code);
            return true;
        }
        return false;
    }

}

