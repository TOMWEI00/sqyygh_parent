package com.tomwei.sqyygh.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tomwei.sqyygh.model.user.Patient;

public interface PatientMapper extends BaseMapper<Patient> {
}
