package com.tomwei.sqyygh.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tomwei.sqyygh.model.user.UserInfo;

public interface UserInfoMapper extends BaseMapper<UserInfo> {
}
