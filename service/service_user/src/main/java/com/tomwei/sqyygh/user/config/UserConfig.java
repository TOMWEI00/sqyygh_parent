package com.tomwei.sqyygh.user.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.tomwei.sqyygh.user.mapper")
public class UserConfig {
}
