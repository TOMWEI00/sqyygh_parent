package com.tomwei.sqyygh.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tomwei.sqyygh.model.order.PaymentInfo;

public interface PaymentMapper extends BaseMapper<PaymentInfo> {
}
