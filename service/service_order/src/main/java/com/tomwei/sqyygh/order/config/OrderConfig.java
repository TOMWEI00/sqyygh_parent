package com.tomwei.sqyygh.order.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@MapperScan("com.tomwei.sqyygh.order.mapper")
public class OrderConfig {

}
