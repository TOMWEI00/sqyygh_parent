package com.tomwei.sqyygh.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tomwei.sqyygh.model.order.RefundInfo;

public interface RefundInfoMapper extends BaseMapper<RefundInfo> {
}
