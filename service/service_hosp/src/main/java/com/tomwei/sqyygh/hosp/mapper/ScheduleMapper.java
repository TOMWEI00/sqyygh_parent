package com.tomwei.sqyygh.hosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tomwei.sqyygh.model.hosp.Schedule;

public interface ScheduleMapper extends BaseMapper<Schedule> {
}
