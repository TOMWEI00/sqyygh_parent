package com.tomwei.sqyygh.hosp.controller;

import com.tomwei.sqyygh.common.result.Result;
import com.tomwei.sqyygh.hosp.service.DepartmentService;
import com.tomwei.sqyygh.vo.hosp.DepartmentVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admin/hosp/department")
//@CrossOrigin
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    //根据医院编号，查询医院所以科室列表
    @ApiOperation(value = "根据医院编号，查询医院所以科室列表")
    @GetMapping("getDeptList/{hoscode}")
    public Result getDeptList(@PathVariable String hoscode){
        List<DepartmentVo> list = departmentService.findDeptTree(hoscode);
         return Result.ok(list);
    }

}
