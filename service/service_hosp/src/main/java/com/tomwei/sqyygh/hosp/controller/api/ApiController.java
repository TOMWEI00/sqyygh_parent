package com.tomwei.sqyygh.hosp.controller.api;

import com.tomwei.sqyygh.common.exception.SqyyghException;
import com.tomwei.sqyygh.common.helper.HttpRequestHelper;
import com.tomwei.sqyygh.common.result.Result;
import com.tomwei.sqyygh.common.result.ResultCodeEnum;
import com.tomwei.sqyygh.common.utils.MD5;
import com.tomwei.sqyygh.hosp.service.DepartmentService;
import com.tomwei.sqyygh.hosp.service.HospitalService;
import com.tomwei.sqyygh.hosp.service.HospitalSetService;
import com.tomwei.sqyygh.hosp.service.ScheduleService;
import com.tomwei.sqyygh.model.hosp.Department;
import com.tomwei.sqyygh.model.hosp.Hospital;
import com.tomwei.sqyygh.model.hosp.Schedule;
import com.tomwei.sqyygh.vo.hosp.DepartmentQueryVo;
import com.tomwei.sqyygh.vo.hosp.ScheduleQueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("api/hosp")
//@CrossOrigin
public class ApiController {

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalSetService hospitalSetService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ScheduleService scheduleService;

    //删除排班接口
    @PostMapping("schedule/remove")
    public Result removeSchedule(HttpServletRequest request){
        //获取传递过来的科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //医院编号
        String hoscode = (String)paramMap.get("hoscode");

        //科室编号
        String hosScheduleId = (String)paramMap.get("hosScheduleId");

        //签名校验
        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }

        scheduleService.remove(hoscode,hosScheduleId);
        return Result.ok();
    }
    //查询排班接口
    @PostMapping("schedule/list")
    public Result findSchedule(HttpServletRequest request){
        //获取传递过来的科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //医院编号
        String hoscode = (String)paramMap.get("hoscode");
        //科室编号
        String depcode = (String)paramMap.get("depcode");

        //当前页
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 : Integer.parseInt((String)paramMap.get("page"));
        //每页记录数
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 1 : Integer.parseInt((String)paramMap.get("limit"));
        //签名校验
        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }

        ScheduleQueryVo scheduleQueryVo = new ScheduleQueryVo();
        scheduleQueryVo.setHoscode(hoscode);
        scheduleQueryVo.setDepcode(depcode);

        //调用方法
        Page<Schedule> PageModel = scheduleService.findPageSchedule(page,limit,scheduleQueryVo);
        return Result.ok(PageModel);

    }

    //上传排班接口
    @PostMapping("saveSchedule")
    public Result saveSchedule(HttpServletRequest request){
        //获取传递过来的排班信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);
        //医院编号
        String hoscode = (String)paramMap.get("hoscode");

        //签名校验
        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }
        scheduleService.save(paramMap);
        return Result.ok();
    }

    //删除科室接口
    @PostMapping("department/remove")
    public Result removeDepartment(HttpServletRequest request){
        //获取传递过来的科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //医院编号
        String hoscode = (String)paramMap.get("hoscode");

        //科室编号
        String depcode = (String)paramMap.get("depcode");

        //签名校验
        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }

        departmentService.remove(hoscode,depcode);
        return Result.ok();
    }
    //查询科室接口
    @PostMapping("department/list")
    public Result findDepartment(HttpServletRequest request){
        //获取传递过来的科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //医院编号
        String hoscode = (String)paramMap.get("hoscode");
        //当前页
        int page = StringUtils.isEmpty(paramMap.get("page")) ? 1 : Integer.parseInt((String)paramMap.get("page"));
        //每页记录数
        int limit = StringUtils.isEmpty(paramMap.get("limit")) ? 1 : Integer.parseInt((String)paramMap.get("limit"));
        //签名校验
        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }

        DepartmentQueryVo departmentQueryVo = new DepartmentQueryVo();
        departmentQueryVo.setHoscode(hoscode);

        //调用方法
        Page<Department> PageModel = departmentService.findPageDepartment(page,limit,departmentQueryVo);
        return Result.ok(PageModel);
    }

    //上传科室接口
    @PostMapping("saveDepartment")
    public Result saveDepartment(HttpServletRequest request){
        //获取传递过来的科室信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //获取科室编号
        String hoscode = (String) paramMap.get("hoscode");

        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的科室编码，查询数据库，查询签名
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }
        //调用service
        departmentService.save(paramMap);
        return Result.ok();
    }
    //查询医院
    @PostMapping("hospital/show")
    public Result getHospital(HttpServletRequest request) {
        //获取传递过来的医院信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);
        //获取医院编号
        String hoscode = (String) paramMap.get("hoscode");

        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //调用service方法，根据医院编号查询
        Hospital hospital = hospitalService.getByHoscode(hoscode);
        return Result.ok(hospital);
    }

    //上传医院接口
    @PostMapping("saveHospital")
    public Result saveHosp(HttpServletRequest request) {
        //获取传递过来的医院信息
        Map<String, String[]> requestMap = request.getParameterMap();
        Map<String, Object> paramMap = HttpRequestHelper.switchMap(requestMap);

        //1.获取医院系统传递过来的签名，签名进行MD5加密
        String hospSing = (String) paramMap.get("sign");

        //2.根据传递过来的医院编码，查询数据库，查询签名
        String hoscode = (String) paramMap.get("hoscode");
        String signKey = hospitalSetService.getSignKey(hoscode);

        //3.把数据库传来的签名进行MD5加密
        String signKeyMd5 = MD5.encrypt(signKey);

        //4.判断签名是否一致
        if (!hospSing.equals(signKeyMd5)) {
            throw new SqyyghException(ResultCodeEnum.SIGN_ERROR);
        }

        //传输过程中“+”转换为了“ ”，因此我们要转换回来
        String logoData = (String) paramMap.get("logoData");
        logoData = logoData.replaceAll(" ", "+");
        paramMap.put("logoData", logoData);
        //调用service方法
        hospitalService.save(paramMap);
        return Result.ok();
    }
}
