package com.tomwei.sqyygh.hosp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tomwei.sqyygh.model.hosp.HospitalSet;


public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
