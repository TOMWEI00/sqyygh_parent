package com.tomwei.sqyygh.common.exception;

import com.tomwei.sqyygh.common.result.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Result error(Exception e){
        e.printStackTrace();
        return Result.fail();
    }

    @ExceptionHandler(SqyyghException.class)
    public Result error(SqyyghException e){
        e.printStackTrace();
        return Result.fail();
    }
}
